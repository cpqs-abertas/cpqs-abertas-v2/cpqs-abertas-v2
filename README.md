# CPQs Abertas

## Sobre o projeto

O projeto CPQs Abertas surgiu no segundo semestre de 2019, como FAU Aberta, em um trabalho para a disciplina de [Laboratório de Métodos Ágeis],() ministrada no Instituto de Matemática e Estatística da Universidade de São Paulo, para expor o trabalho acadêmico realizado na Faculdade de Arquitetura e Urbanismo (FAU).

Em 2021, o projeto evoluiu buscando abranger todas as unidades da USP. O intuito do projeto é aumentar a visibilidade das produções realizadas por cada unidade da USP, atráves da análise dos dados retirados da [Plataforma Lattes](http://lattes.cnpq.br/).

## Explorando os Sites:

Para explorar os sites disponíveis, acesse a sessão [CPQs-Abertas](http://hub-cpqs-abertas.s3-website-sa-east-1.amazonaws.com/) e clique nos institutos disponíveis para explorar a produção intelectual da nossa comunidade acadêmica.

Para saber mais sobre a parte técnica do projeto, confira também a nossa [Wiki](https://gitlab.com/cpqs-abertas/cpqs-abertas-v2/cpqs-abertas-v2/-/wikis/home).
