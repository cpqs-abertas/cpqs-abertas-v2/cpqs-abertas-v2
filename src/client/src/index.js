import React, { lazy, Suspense } from "react";
import ReactDOM from "react-dom";

const App = lazy(() => {
  return import(`./institutos/App`);
});

ReactDOM.render(
  <Suspense fallback={<div></div>}>
    <App />
  </Suspense>,
  document.getElementById("root")
);
