import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import WorldMap from "./WorldMap";

function WorldMapContainer(props) {
  const { fetchData, formatData, color, Loading } = props;
  const [map, setMap] = useState(null);

  useEffect(() => {
    fetchData().then((data) => {
      setMap(formatData(data));
    });
  }, []);

  return <>{!map ? <Loading /> : <WorldMap countries={map} color={color} />}</>;
}

WorldMapContainer.propTypes = {
  color: PropTypes.string,
  fetchData: PropTypes.func.isRequired,
  formatData: PropTypes.func,
  Loading: PropTypes.func,
};

WorldMapContainer.defaultProps = {
  formatData: (data) => data,
  Loading: () => <></>,
};

export default WorldMapContainer;
