import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import NationalMap from "./NationalMap";

function NationalMapContainer(props) {
  const { fetchData, formatData, color, Loading } = props;
  const [map, setMap] = useState(null);

  useEffect(() => {
    fetchData().then((data) => {
      setMap(formatData(data));
    });
  }, []);

  return <>{!map ? <Loading /> : <NationalMap states={map} color={color} />}</>;
}

NationalMapContainer.propTypes = {
  color: PropTypes.string,
  fetchData: PropTypes.func.isRequired,
  formatData: PropTypes.func,
  Loading: PropTypes.func,
};

NationalMapContainer.defaultProps = {
  formatData: (data) => data,
  Loading: () => <></>,
};

export default NationalMapContainer;
