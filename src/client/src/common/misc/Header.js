/* eslint-disable */
/* eslint-disable react/jsx-key */
import React, { useContext } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Link } from "react-router-dom";
import BreadcrumbWrapper from "../../institutos/components/wrappers/BreadcrumbWrapper";
import InstituteStylesContext from "../../institutos/context/InstituteStyles";
import warnImg from "../assets/images/in_development_warn.jpeg";
function Header(props) {
  const {
    // title,
    // titleColor,
    // subtitle,
    // subtitleColor,
    // fontFamily,
    instituteLogo,
    menuFilter,
    // logoWidth,
    // logoHeight,
  } = props;

  const urlCMS = process.env.REACT_APP_BACKEND_S3;
  const instituteStyle = useContext(InstituteStylesContext);
  let docentesStyle = "unselected";
  let unityStyle = "unselected";
  let faqStyle = "unselected";

  const styles = {
    bordered: {
      border: "2.5px solid #000000",
    },
    selected: {
      backgroundColor: instituteStyle.titulo_Cor,
      borderColor: "transparent",
      color: "#FFFFFF",
      fontWeight: "700",
    },
    unselected: {},
  };

  if (menuFilter.unidade) {
    unityStyle = "selected";
  } else {
    if (menuFilter.docentes) {
      docentesStyle = "selected";
    } else {
      if (menuFilter.faq) {
        faqStyle = "selected";
      }
    }
  }

  const Container = styled.div`
    display: flex;
    padding: 76px 72px 0px 0px;
    grid-column-start: 2;
    overflow: hidden;
    width: 1440px;
    height: 212px;
    # background-color: yellow;
    flex-wrap: wrap;
  `;

  const HeaderMenu = styled.div`
    display: flex;
    align-items: center;
    height: 60px;
    gap: 16px;
    > a {
      align-items: center;
      background-color: ${instituteStyle.menu.backgroundColor};
      border: 1px solid;
      border-radius: ${instituteStyle.menu.borderRadius}px;
      color: ${instituteStyle.menu.fontColor};
      cursor: pointer;
      font-family: ${instituteStyle.menu.fontFamily};
      display: flex;
      font-size: ${instituteStyle.menu.fontSize}px;
      font-weight: ${instituteStyle.menu.fontWeight};
      height: 44px;
      justify-content: space-around;
      text-align: center;
      text-decoration: none;
      width: 331px;
      # background-color: green;
      > p {
        font-size: 20px;
      }
  `;

  const FAQButton = styled.button`
    :hover {
      background-color: ${(prop) =>
        prop.hoverBackgroundColor || instituteStyle.menu.hoverBackgroundColor};
      border-color: transparent !important;
      color: #ffffff;
    }
  `;

  const Logo = styled.div`
    align-items: center;
    # background-color: magenta;
    padding-left: 100px;
    height: 60px;
    width: 340px;
  `;

  const HeaderLink = styled(Link)`
    
    :hover {
      background-color: ${(prop) =>
        prop.hoverBackgroundColor || instituteStyle.menu.hoverBackgroundColor};
      border-color: transparent !important;
      color: #ffffff;
    }
  `;

  return (
    <Container>
        <Logo>
          <img
            width="auto"
            height={60}
            src={`${urlCMS}${instituteLogo}-logo.png`}
            style={{
              maxWidth: "80%",
              objectFit: "contain",
            }}
            alt="logo"
          />
        </Logo>
        <HeaderMenu>
          <HeaderLink type="button" to="/" style={styles[unityStyle]}>
            <p>Unidade</p>
          </HeaderLink>

          <HeaderLink
            type="button"
            to="/pessoa/nomes"
            style={styles[docentesStyle]}
          >
            <p>Docentes</p>
          </HeaderLink>
          <FAQButton type="button" as={Link} to="/faq" style={styles[faqStyle]}>
            <p>FAQ</p>
          </FAQButton>
      </HeaderMenu>
      <BreadcrumbWrapper />
    </Container>
  );
}

Header.propTypes = {
  fontFamily: PropTypes.string,
  instituteLogo: PropTypes.string.isRequired,
  // logoHeight: PropTypes.number.isRequired,
  // logoWidth: PropTypes.number.isRequired,
  subtitle: PropTypes.string.isRequired,
  subtitleColor: PropTypes.string,
  title: PropTypes.string.isRequired,
  titleColor: PropTypes.string,
  setMenuFilter: PropTypes.func.isRequired,
  menuFilter: PropTypes.object.isRequired,
};

Header.defaultProps = {
  fontFamily: "sans-serif",
  subtitleColor: "#000000",
  titleColor: "#000000",
};

export default Header;
