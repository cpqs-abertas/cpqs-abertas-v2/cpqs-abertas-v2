/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
// import {
  //Container,
  //ContainerBottom,
  //HubLink,

// } from "../../institutos/styles.js";

function Footer(props) {
  const { children, font } = props;

  return (
    <Container style={{ fontFamily: font }}>
      <ContainerBottom>
        <HubLink>
          <a href="http://hub-uspaberta.s3-website-us-east-1.amazonaws.com">
            HUB DE INSTITUTOS
          </a>
        </HubLink>
        {children}
      </ContainerBottom>
    </Container>
  );
}

Footer.propTypes = {
  children: PropTypes.node.isRequired,
  font: PropTypes.string,
};

Footer.defaultProps = {
  font: "sans-serif",
};

export default Footer;

const Container = styled.div`
  grid-column-start: 2;
  grid-row-start: 3;
  width: 1440px;
  height: 136px;
  display: flex;
  align-items: center;
  # background-color: pink;
`;

// const ContainerTop = styled.div`
//   text-align: right;
// `;

const ContainerBottom = styled.div`
  display: flex;
  overflow: hidden;
  padding-top: 30px;
  width: 100%;
  justify-content: space-between;
  > a {
    font-weight: 700;
  }
`;

const HubLink = styled.div`
  color: black;
  font-weight: bold;
  padding-left: 76px;

  > a {
    text-decoration: underline;
    color: black;
    font-size: 24px;
  }
`;
