/* eslint-disable */
import React, { useContext } from "react";
import warnImg from "../assets/images/in_development_warn.jpeg";
import { BannerStyle } from "../../institutos/styles";
import InstituteStylesContext from "../../institutos/context/InstituteStyles";

function DevelopmentWarn() {
  const instituteStyle = useContext(InstituteStylesContext);
  return ((instituteStyle.emDesenvolvimento ? 
    (
    <BannerStyle>
      <img src={warnImg} alt="In Development" />
    </BannerStyle>
    ) : 
    null)
  );
}

export default DevelopmentWarn;
