import React from "react";
import PropTypes from "prop-types";
import styled, { keyframes } from "styled-components";

function Fader(props) {
  const { image } = props;
  const urlCMS = process.env.REACT_APP_BACKEND_S3;

  const selectFaderImage = () =>
    image ? `${urlCMS}${image}-logo.png` : `${urlCMS}cpqs-abertas-logo.png`;

  return (
    <Container>
      <Image
        style={{ maxWidth: "10%", objectFit: "contain" }}
        src={selectFaderImage()}
        alt="loading"
      />
    </Container>
  );
}

Fader.propTypes = {
  image: PropTypes.string.isRequired,
};

const fade = keyframes`
  0% {
    opacity: 1;
  }

  50% {
    opacity: 0;
  }

  100% {
    opacity: 1;
  }
`;

const Container = styled.div`
  align-items: center;
  display: flex;
  height: 245px;
  justify-content: center;
`;

const Image = styled.img`
  -webkit-animation: ${fade} 2s linear infinite;
  -moz-animation: ${fade} 2s linear infinite;
  animation: ${fade} 2s linear infinite;
`;

export default Fader;
