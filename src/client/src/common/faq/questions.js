const questions = [
  {
    text: "O que é o projeto CPqs abertas?",
    answer: `O intuito principal deste projeto é dar visibilidade quantitativa e
      qualitativamente à produção intelectual da faculdade, difundindo sua
      especificidade e diversidade através de dados extraídos do currículo
      Lattes de seus docentes, tanto para a comunidade interna, a USP,
      quanto para a comunidade externa, contribuindo assim para uma melhor
      compreensão da sociedade em geral quanto ao trabalho e às contribuições
      advindas das atividades desenvolvidas na Universidade.`,
  },
  {
    text: "Como são realizadas as coletas de informações do site?",
    answer: `Os dados exibidos são provenientes do Lattes. nos casos de mapas, usamos
      como referência a capital dos estados (ou países, se for o caso).`,
  },
  {
    text: "O quão precisos são os dados utilizados?",
    answer: `Capturamos os dados públicos dos CV Lattes periodicamente, sendo que a
      data da coleta está indicada na página de abertura.`,
  },
  {
    text: "A quais institutos o projeto se vincula hoje?",
    answer: `O projeto começou pela FAU e a segunda unidade a ser atendida será a
      FEA-RP.`,
  },
  {
    text: `Percebi incongruências nos meus dados apresentados na plataforma, o que
      devo fazer?`,
    answer: `Reveja o preenchimento de seu CV Lattes. A completude e a precisão neste
      preenchimento são fundamentais para a congruência dos dados apresentados.
      A próxima atualização do sistema deve apresentar as devidas correções.`,
  },
];

export default questions;
