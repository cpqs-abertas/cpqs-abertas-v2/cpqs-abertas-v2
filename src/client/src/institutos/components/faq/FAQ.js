import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import FAQForm from "./FAQForm";
import FAQQuestionItem from "./FAQQuestionItem";
import questions from "../../../common/faq/questions";
import { CenteredColumn } from "../../styles";
import { postQuestion } from "../../../common/API";
import { setFilters } from "../../helpers";

function FAQ(props) {
  const { setMenuFilter } = props;
  const [selectedQuestionIndex, setSelectedQuestionIndex] = useState(-1);

  const questionsPlusForm = [];
  questions.forEach((pair) => {
    questionsPlusForm.push(pair);
  });

  useEffect(() => {
    setFilters(setMenuFilter, { faq: true }, (filter, newState) => {
      newState[filter] = false;
    });
  }, []);

  questionsPlusForm.push({
    text: "Minha dúvida não está aqui. O que fazer?",
    answer: (
      <>
        Caso você tenha alguma dúvida que não tenha sido respondida, não hesite
        em nos contatar! Basta preencher as informações abaixo que te mandaremos
        um e-mail esclarecendo quaisquer perguntas
        <FAQForm postQuestion={postQuestion} />
      </>
    ),
  });

  const questionCount = questionsPlusForm.length;
  const questionItems = questionsPlusForm.map((question, index) => (
    <FAQQuestionItem
      text={question.text}
      answer={question.answer}
      number={index}
      questionCount={questionCount}
      selectedQuestionNumber={selectedQuestionIndex}
      setSelectedQuestionNumber={setSelectedQuestionIndex}
    />
  ));

  return (
    <CenteredColumn style={{ zIndex: "10" }}>
      <ContentArea>
        <FAQArea>
          <FAQTitle>
            <span>FAQ</span>
          </FAQTitle>
          <QuestionList>{questionItems}</QuestionList>
        </FAQArea>
      </ContentArea>
    </CenteredColumn>
  );
}

FAQ.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

const ContentArea = styled.div`
  background: rgba(255, 255, 255, 0.75);
  height: 100%;
  width: 100%;
  text-align: start;
`;

const FAQArea = styled.div`
  background: #ffffff;
  height: 100%;
  padding: 20px 20px 20px 10px;
  width: 50%;
`;

const FAQTitle = styled.div`
  align-items: center;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: -4px 0 0 0;
  padding: 0 0 10px 0;
  width: 100%;

  > span {
    font-size: 25px;
    font-weight: bold;
  }
`;

const QuestionList = styled.ul`
  font-size: 15px;
  list-style-type: none;
  margin: 0.25rem 0 0 0;
  width: 758px;
  padding: 0;
`;

export default FAQ;
