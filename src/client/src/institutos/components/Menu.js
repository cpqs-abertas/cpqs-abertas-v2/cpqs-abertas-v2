/* eslint-disable */
/* eslint-disable react/jsx-key */
import React, { useContext, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Link, useLocation } from "react-router-dom";
import InstituteStylesContext from "../context/InstituteStyles";
import { setFilters } from "../helpers";
import { Tooltip } from "react-tooltip";


const setStyleForProductionTypes = (
  productionTypes,
  ProductionStyles,
  style
) => {
  for(let i = 0; i < productionTypes.length; i++) {
    ProductionStyles[productionTypes[i]] = style
  }
}

const setStyleForList = ( 
  objects,
  styles,
  indexToSkip,
  style
) => {
  for (let i = 0; i < objects.length; i++) {
    if (i !== indexToSkip) {
      styles[objects[i]] = style
    }
  }
}



function Menu(props) {
  const { menuFilter } = props;
  const { pathname } = useLocation();
  const lattesIdMatch = /\d+/.exec(pathname);
  const instituteStyle = useContext(InstituteStylesContext);

  let lattesId = "";
  if (lattesIdMatch) {
    [lattesId] = lattesIdMatch;
  }

  const styles = {
    bordered: { 
      border: "4px solid #000000",
    },
    selected: { 
      backgroundColor: instituteStyle.titulo_Cor,
      borderColor: "transparent",
      color: "#FFFFFF",
      fontWeight: "700",
    },
    unselected: {}, 
    hidden: {
      display: "none",
    },
    teste: {
      backgroundColor: "yellow",
    }
  };

  const stylesIsFaq = {
    selected: {
      borderRadius: "1.19px",
      borderColor: "#848484",
      color: "#848484",
    },
    initialButton: {
      borderRadius: "1.19px",
      borderColor: "#848484",
      color: "white",
      backgroundColor: "#848484",
      fontWeight: "700",
    },
    unselected: {},
  };

  // let unityStyle = "unselected";
  // let homeStyle = "unselected";
  // let docentesStyle = "unselected";
  let menuFaqStyle = "unselected";
  if(menuFilter.faq){
    menuFaqStyle = "selected";
  }


  
  /* --------------------------------------------------------------- */
  
  /* Estilo dos botões das Produções: */
  
  const productionsStyles = {
    bibliografica: "unselected",
    artistica: "unselected",
    tecnica: "unselected",
    orientacoes: "unselected",
    bancas: "unselected",
    premios: "unselected",
  };
  
  /* Lista dos botões da liista de produções:  */
  const productionTypes = Object.keys(productionsStyles);
  
  /* Produção selecionada: */
  const selectedProductionType = productionTypes.find(
    (productionType) => menuFilter[productionType]
  );

  /* Definição do texto e links dos botões das produções: */
  let productionTypeInfo = {
    bibliografica: { text: "Produção Bibliográfica", urlName: "producao-bibliografica" },
    tecnica: { text: "Produção Técnica", urlName: "producao-tecnica" },
    orientacoes: { text: "Orientações", urlName: "orientacoes" },
    bancas: { text: "Bancas", urlName: "bancas" },
    premios: { text: "Prêmios e Títulos", urlName: "premios-e-titulos" },
  };

  /* Definição do texto e link do botão da Produção Artística (se tiver): */
  if (instituteStyle.menu.prodArtistica) {
    productionTypeInfo = {
      artistica: {
        text: "Produção Artística",
        urlName: "producao-artistica",
      },
      ...productionTypeInfo,
    };
  }

  /* --------------------------------------------------------------- */

  /* Estilo dos botões de departamentos */
  let departmentsStyles = {};

  /* Estilo dos botões dos Departamentos: */
  const departments = Object.keys(instituteStyle.departments);

  
  /* Estilo para botões selecionados: */
  Object.keys(instituteStyle.departments).map((key) => {
    departmentsStyles[key] = "unselected";
    styles[key] = {  
      backgroundColor: instituteStyle.departments[key].color,
      borderColor: "transparent",
      color: "#FFFFFF",
    };
  });


  /* --------------------------------------------------------------- */
  /* Regras para estilos */

  /* Seleção do botão de docentes */
  if(menuFilter.docentes) {
    setStyleForList(
      departments,
      departmentsStyles,
      -1,
      "bordered"
    )
  }

  /* Seleção do botão do departamento: */
  for (let i = 0; i < departments.length; i++) {
    const department = departments[i];
    if (menuFilter[department]) {
      departmentsStyles[department] = department;
      if (menuFilter.perfil) {
        setStyleForList(
          departments,
          departmentsStyles,
          i,
          "unselected"
        );
      }
      break;
    }
  }

  /* Seleção de algum perfil: */
  if (menuFilter.perfil) {
    setStyleForList(
      productionTypes,
      productionsStyles,
      -1,
      "bordered"
    )
    setStyleForList(
      departments,
      departmentsStyles,
      -1,
      "hidden"
    )
  } else {
    setStyleForList(
      productionTypes,
      productionsStyles,
      -1, 
      "hidden"
    )
  }

  /* Estilo para botão selecionado de produção: */
  if (selectedProductionType) {
    productionsStyles[selectedProductionType] = "selected";
  }

  /* Estilização dos botões: */
  const Container = styled.div`
    margin-top: 16px;
    display: flex;
    flex-direction: column;
    grid-column-start: 2;
    grid-row-start: 2;
    align-items: center;
  `;


  const MenuLink = styled(Link)`
    align-items: center;
    background-color: ${instituteStyle.menu.backgroundColor};
    border: 1px solid;
    border-radius: ${instituteStyle.menu.borderRadius}px;
    color: ${instituteStyle.menu.fontColor};
    cursor: pointer;
    font-family: ${instituteStyle.menu.fontFamily};
    display: flex;
    font-size: ${instituteStyle.menu.fontSize}px;
    font-weight: ${instituteStyle.menu.fontWeight};
    height: 48px;
    justify-content: space-around;
    text-decoration: none;
    width: 222px;
    margin-bottom: 16px;
    :hover {
      background-color: ${(prop) =>
        prop.hoverBackgroundColor || instituteStyle.menu.hoverBackgroundColor};
      border-color: transparent !important;
      color: #ffffff;
    }
  `;

  const MenuDepartments = styled(Link)`
    :hover {
      background-color: ${(prop) =>
        prop.hoverBackgroundColor || instituteStyle.menu.hoverBackgroundColor};
      border-color: transparent !important;
      color: #ffffff;
    }
  `;

  const MenuButton = styled.button`
    :hover {
      background-color: ${(prop) =>
        prop.hoverBackgroundColor || instituteStyle.menu.hoverBackgroundColor};
      border-color: transparent !important;
      color: #ffffff;
    }
  `;

  const [isMenuSelected, setIsMenuSelected] = useState(false);
  const [isProdSelected, setIsProdSelected] = useState(false);
  
  const Clicado = (variavel, setVariavel) => {
    setVariavel(!variavel);
  }

  const MenuPrincipal = styled.div`
    align-items: center;
    background-color: ${instituteStyle.titulo_Cor};
    color: white;
    border: 1px solid black;
    cursor: pointer;
    display: flex;
    height: 48px;
    justify-content: space-around;
    width: 222px;
    font-size: 20px;
    font-weight: bold;
    margin-bottom: 16px;
    text-decoration: none;
  `;

  const DropButtons = styled.div`
    display: ${props => props.isHidden ? "none" : "block"};
  `;

  /* Retorno dos botões: */
  return (
    <Container
      id="links"
      fontFamily={instituteStyle.menu.fontFamily}
      fontSize={instituteStyle.menu.fontSize}
    >
      {!menuFilter.perfil ? (
        <MenuPrincipal onClick={() => Clicado(isMenuSelected, setIsMenuSelected)} to="/">{instituteStyle.acronym} USP</MenuPrincipal>
        ) : null}

      {Object.keys(instituteStyle.departments).map((key) => {
        return (
          <DropButtons isHidden={!isMenuSelected}>
            <MenuLink
              hoverBackgroundColor={instituteStyle.departments[key].color}
              id={key}
              to={menuFilter.docentes ? `/pessoa/nomes/${key}` : `/${key}`}
              style={
                menuFaqStyle === "selected"
                  ? stylesIsFaq[menuFaqStyle]
                  : styles[departmentsStyles[key]]} 
            >
              {key}
            </MenuLink>
          </DropButtons>
        );
      })}

      {menuFilter.perfil ? (
        <MenuPrincipal onClick={() => Clicado(isProdSelected, setIsProdSelected)} to="/">Produções</MenuPrincipal> 
        ) : null}
   
      {Object.keys(productionTypeInfo).map((productionType) => {
        const { text, urlName } = productionTypeInfo[productionType];
        return (
          <DropButtons isHidden={!isProdSelected}>
            <MenuLink
              to={
                menuFilter.perfil
                  ? `/pessoa/${urlName}/${lattesId}`
                  : `/${urlName}`
              }
              style={
                menuFaqStyle === "selected"
                  ? stylesIsFaq[menuFaqStyle]
                  : styles[productionsStyles[productionType]]}
            >
              {text}
            </MenuLink>
          </DropButtons>
        );
      })}
    </Container>
  );
}

Menu.propTypes = {
  menuFilter: PropTypes.object.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

export default Menu;