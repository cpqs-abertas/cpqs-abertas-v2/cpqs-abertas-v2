/* eslint-disable */

import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";
// import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
  Conteudo,
  Title,
  DivCard,
  DivGraph,
  DivInfoProducao,
  DivInfoText,
  DivTotal,
  BarChartWrapper,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
} from "../../styles";
import BarChartContainer from "../../../common/charts/BarChartContainer";
import LineChartContainer from "../../../common/charts/LineChartContainer";
import NationalMapContainer from "../../../common/maps/NationalMapContainer";
import WordCloudContainer from "../../../common/keywords/WordCloudContainer";
import WorldMapContainer from "../../../common/maps/WorldMapContainer";
import FaderWrapper from "../wrappers/FaderWrapper";
import CarouselWrapper from "../wrappers/CarouselWrapper";
import { getDashboard } from "../../../common/API";
import {
  createCountByCategoryByLabel,
  createCountByYearByDep,
  createInstituteCountByYear,
} from "../../helpers";
import InstituteStylesContext from "../../context/InstituteStyles";
// import { Title } from "chart.js";
// import Fader from "../../../common/misc/Fader";

function Activity(props) {
  const instituteStyle = useContext(InstituteStylesContext);
  const departments = Object.keys(instituteStyle.departments);
  const barColors = [
    instituteStyle.graphs.barChart.color1,
    instituteStyle.graphs.barChart.color2,
    instituteStyle.graphs.barChart.color3,
    instituteStyle.graphs.barChart.color4,
    instituteStyle.graphs.barChart.color5,
    instituteStyle.graphs.barChart.color6,
    instituteStyle.graphs.barChart.color7,
    instituteStyle.graphs.barChart.color8,
  ];
  const nuvemColors = [
    instituteStyle.graphs.worldCloud.color1,
    instituteStyle.graphs.worldCloud.color2,
    instituteStyle.graphs.worldCloud.color3,
    instituteStyle.graphs.worldCloud.color4,
  ];
  const {
    chartsToRemove,
    getCount,
    getCountByType,
    getKeywords,
    getMap,
    getNationalMap,
    infoText,
    type,
    updateMenuEntry,
  } = props;
  const [countByProductionType, setCountByProductionType] = useState(null);
  const chartsMap = {
    wordCloud: true,
    worldMap: true,
    nationalMap: true,
    barChart: true,
    InsituteLineChart: true,
    depsLineChart: true,
  };
  chartsToRemove.forEach((chart) => {
    chartsMap[chart] = false;
  });
  useEffect(() => {
    updateMenuEntry();

    getDashboard().then((responsePayload) => {
      setCountByProductionType(responsePayload);
    });
  }, []);
  if (!countByProductionType) {
    return (
      <Conteudo>
        <FaderWrapper/>
      </Conteudo>
    );
  }
  const charts = {
    wordCloud: (
      <WordCloudWrapper>
        <WordCloudContainer
          colors={nuvemColors}
          fetchData={() => getKeywords({ limit: 50 })}
          Loading={FaderWrapper}
        />
      </WordCloudWrapper>
    ),
    worldMap: (
      <WorldMapWrapper>
        <WorldMapContainer
          fetchData={() => getMap()}
          Loading={FaderWrapper}
          color={instituteStyle.graphs.map.color}
        />
      </WorldMapWrapper>
    ),
    nationalMap: (
      <NationalMapWrapper>
        <NationalMapContainer
          fetchData={() => getNationalMap()}
          Loading={FaderWrapper}
          color={instituteStyle.graphs.map.color}
        />
      </NationalMapWrapper>
    ),
    barChart: (
      <BarChartWrapper>
        <BarChartContainer
          labels={instituteStyle.deptLabels?.list}
          colors={barColors}
          formatData={createCountByCategoryByLabel}
          fetchData={async () => {
            const countByYearByProductionTypeByDep = await getCount({
              ano_inicio: 1978,
            });
            const categories = Object.keys(
              Object.values(countByYearByProductionTypeByDep)[0]
            );
            return {
              categories,
              countByYearByCategoryByLabel: countByYearByProductionTypeByDep,
            };
          }}
          Loading={FaderWrapper}
        />
      </BarChartWrapper>
    ),
    InsituteLineChart: (
      <LineChartWrapper>
        <LineChartContainer
          labels={[instituteStyle.nome]}
          color={{ line1: instituteStyle.graphs.lineChart.color }}
          fill={true}
          formatData={createInstituteCountByYear}
          fetchData={() => getCount({ ano_inicio: 1978 })}
          Loading={FaderWrapper}
        />
      </LineChartWrapper>
    ),
    depsLineChart: (
      <LineChartWrapper>
        <LineChartContainer
          labels={departments}
          color={{
            line1: instituteStyle.graphs.barChart.color1,
            line2: instituteStyle.graphs.barChart.color2,
            line3: instituteStyle.graphs.barChart.color3,
          }}
          fill={false}
          formatData={createCountByYearByDep}
          fetchData={() => getCount({ ano_inicio: 1978 })}
          Loading={FaderWrapper}
        />
      </LineChartWrapper>
    ),
  };
  const carouselItems = [];
  Object.keys(chartsMap).forEach((chart) => {
    if (chartsMap[chart]) {
      carouselItems.push(charts[chart]);
    }
  });
  return (
    <Conteudo>
      <div id="left"> 
        <DivInfoProducao
          style={{
            fontFamily: `${instituteStyle.fontFamily}`,
            color: `${instituteStyle.menu.fontColor}`,
          }}
        >
          <Title>{type}</Title>
          <DivTotal>
            <span id="totalProd">
              {Intl.NumberFormat().format(countByProductionType[type])}
            </span>
            <span id="result">resultados</span>
          </DivTotal>
        </DivInfoProducao>
        <DivGraph>
          <CarouselWrapper>{carouselItems}</CarouselWrapper>
        </DivGraph>
      </div>
      <div id="right">
        <DivInfoText>
            <span className="infotext_title"
            >
              Apresentação
            </span>
            <span className="infotext_content">{infoText}</span>
        </DivInfoText>
      </div>
    </Conteudo>
  );
}

Activity.propTypes = {
  chartsToRemove: PropTypes.array,
  getCount: PropTypes.func,
  getCountByType: PropTypes.func,
  getKeywords: PropTypes.func,
  getMap: PropTypes.func,
  getNationalMap: PropTypes.func,
  infoText: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  updateMenuEntry: PropTypes.func.isRequired,
};

Activity.defaultProps = {
  chartsToRemove: [],
  getCount: () => {},
  getCountByType: () => {},
  getKeywords: () => {},
  getMap: () => {},
  getNationalMap: () => {},
};

export default Activity;
