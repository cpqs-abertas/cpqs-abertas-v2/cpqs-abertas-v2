/* eslint-disable */
/* eslint-disable react/jsx-key */
import { Link, useLocation } from "react-router-dom/cjs/react-router-dom.min";
import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import InstituteStylesContext from "../../context/InstituteStyles";
import PropTypes from "prop-types";
import { getPessoaNames } from "../../../common/API";

const ActiveStyle = styled.span`
  font-weight: 700;
`;

const TextStyle = styled.span`
  text-decoration: underline;
`;

function RenderProducao({ id, nome, departamento, tipo }) {
  return (
    <>
      <Link to={"/pessoa/nomes"}>{"Docentes"}</Link>
      <span>{" > "}</span>
      <Link to={`/pessoa/nomes/${departamento}`}>{`${departamento}`}</Link>
      <span>{" > "}</span>
      <Link to={`/pessoa/perfil/${id}`}>{`${nome}`}</Link>
      <span>{" > "}</span>
      <TextStyle>{"Produção"}</TextStyle>
      <span>{" > "}</span>
      <ActiveStyle>{tipo}</ActiveStyle>
    </>
  );
}

function RenderDocentes({ local, item, key }) {
  const redirect = `/pessoa/nomes/${item}`;
  return (
    <span key={key}>
      {local.pathname.startsWith(`/${item}`) ?
      (
        <span>
          <Link to={`/`}>{`Unidade`}</Link>
          <span>{" > "}</span>
          <ActiveStyle>{`${item}`}</ActiveStyle>
        </span>
      )
        : null}

      {local.pathname === redirect ?
      ( 
        <span>
          <Link to={`/pessoa/nomes/`}>{`Docentes`}</Link>
          <span>{" > "}</span>
          <ActiveStyle>{`${item}`}</ActiveStyle>
        </span>
      )
      : null}
    </span>
  );
}

function BreadcrumbWrapper() {
  const local = useLocation();
  const instituteStyle = useContext(InstituteStylesContext);

  const [professors, setProfessors] = useState(null);

  useEffect(() => {
    getPessoaNames().then((responsePayload) => {
      setProfessors(responsePayload);
    });
  }, []);

  const BreadcrumbStyle = styled.div`
    padding-left: 340px;
    color: #232323;
    font-size: 20px;

    a{
      color: #232323;
    }

    a:link{
      color: #232323;
    }

    a:visited{
      color: #232323;
    }

    a:hover{
      color: #232323;
    }

    a:active{
      color: #232323;
    }
  `;

  return (
    <BreadcrumbStyle>
      <Link to="/">{`${instituteStyle.breadcrumbItem}`}</Link>
      <span>{" > "}</span>
      <ActiveStyle>{local.pathname === "/" ? "Unidade" : null}</ActiveStyle>
      <ActiveStyle>
      {(local.pathname === "/pessoa/nomes" || local.pathname === "/pessoa/nomes/") ? "Docentes" : null}</ActiveStyle>
      <ActiveStyle>{local.pathname === "/faq" ? "FAQ" : null}</ActiveStyle>
      {instituteStyle.deptLabels.list.map((item, index) => (
        <RenderDocentes local={local} item={item} key={index}></RenderDocentes>
      ))}

      {professors &&
        professors.map((professor) => {
          const { nome, departamento, id } = professor;
          const redirect = `/pessoa/perfil/${id}`;
          return (
            <span key={id}>
              {local.pathname === redirect ?
              (
                <>
                  <Link to={"/pessoa/nomes"}>{"Docentes"}</Link>
                  <span>{" > "}</span>
                  <Link to={`/pessoa/nomes/${departamento}`}>{`${departamento}`}</Link>
                  <span>{" > "}</span>
                  <ActiveStyle>{`${nome}`}</ActiveStyle>
                </>
              )
              : null}
              {local.pathname === `/pessoa/producao-artistica/${id}` ?
              (
                <RenderProducao
                  id={id}
                  nome={nome}
                  departamento={departamento}
                  tipo={"Artística"}
                ></RenderProducao>
              )
              : null}
              {local.pathname === `/pessoa/producao-bibliografica/${id}` ?
              (
                <RenderProducao
                  id={id}
                  nome={nome}
                  departamento={departamento}
                  tipo={"Bibliográfica"}
                ></RenderProducao>
              )
              : null}
              {local.pathname === `/pessoa/producao-tecnica/${id}` ?
              (
                <RenderProducao
                  id={id}
                  nome={nome}
                  departamento={departamento}
                  tipo={"Técnica"}
                ></RenderProducao>
              )
              : null}
              {local.pathname === `/pessoa/orientacoes/${id}` ?
              (
                <RenderProducao
                  id={id}
                  nome={nome}
                  departamento={departamento}
                  tipo={"Orientações"}
                ></RenderProducao>
              )
              : null}
              {local.pathname === `/pessoa/bancas/${id}` ?
              (
                <RenderProducao
                  id={id}
                  nome={nome}
                  departamento={departamento}
                  tipo={"Bancas"}
                ></RenderProducao>
              )
              : null}
              {local.pathname === `/pessoa/premios-e-titulos/${id}` ?
              (
                <RenderProducao
                  id={id}
                  nome={nome}
                  departamento={departamento}
                  tipo={"Prêmios e Títulos"}
                ></RenderProducao>
              )
              : null}
            </span>
          );
     })}
    </BreadcrumbStyle>
  );
}

RenderDocentes.propTypes = {
  local: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  key: PropTypes.object.isRequired,
};

RenderProducao.propTypes = {
  id: PropTypes.object.isRequired,
  nome: PropTypes.object.isRequired,
  departamento: PropTypes.object.isRequired,
  tipo: PropTypes.string.isRequired,
};

export default BreadcrumbWrapper;
