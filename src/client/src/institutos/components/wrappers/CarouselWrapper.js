import React, { useContext } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Carousel } from "react-responsive-carousel";
import InstituteStylesContext from "../../context/InstituteStyles";
// import { ReactComponent as ArrowP } from "../../assets/images/Arrow-preenchida.svg";
import { ReactComponent as ArrowC } from "../../assets/images/Arrow-contorno.svg";

function CarouselWrapper(props) {
  const { children } = props;
  const instituteStyle = useContext(InstituteStylesContext);
  // const urlCMS = process.env.REACT_APP_CMS_URL;
  const indicatorStyles = {
    background: "rgba(0, 0, 0, 0.2)",
    width: 8,
    height: 8,
    display: "inline-block",
    margin: "0 10px",
    borderRadius: "50%",
    cursor: "pointer",
  };

  return (
    <Carousel
      showArrows
      showThumbs={false}
      showIndicators={true}
      showStatus={false}
      autoPlay
      infiniteLoop
      interval={5000}
      renderIndicator={(onClickHandler, isSelected, index, label) => {
        if (isSelected) {
          return (
            <li
              style={{
                ...indicatorStyles,
                background: instituteStyle.arrowColor,
              }}
              aria-label={`Selected: ${label} ${index + 1}`}
              title={`Selected: ${label} ${index + 1}`}
            />
          );
        }
        return (
          <li
            style={indicatorStyles}
            onClick={onClickHandler}
            onKeyDown={onClickHandler}
            value={index}
            key={index}
            role="button"
            tabIndex={0}
            title={`${label} ${index + 1}`}
            aria-label={`${label} ${index + 1}`}
          />
        );
      }}
      renderArrowPrev={(clickHandler, hasPrev, label) => {
        if (!hasPrev) {
          return <></>;
        }

        return (
          <CarouselButton
            style={{ left: 0 }}
            aria-label={label}
            onClick={clickHandler}
          >
            <div>
              <ArrowC
                style={{ transform: "rotate(180deg)" }}
                width="21"
                height="21"
                fill={instituteStyle.arrowColor}
              />
            </div>

            {/* <object
              data={urlCMS + instituteStyle.arrow.url}
              width="32"
              height="32"
              onLoad="this.contentDocument.querySelector('svg').fill = 'red'"
            ></object> */}
          </CarouselButton>
        );
      }}
      renderArrowNext={(clickHandler, hasNext, label) => {
        if (!hasNext) {
          return <></>;
        }

        return (
          <CarouselButton
            style={{ right: 0 }}
            aria-label={label}
            onClick={clickHandler}
          >
            <div>
              <ArrowC width="21" height="21" fill={instituteStyle.arrowColor} />
            </div>
          </CarouselButton>
        );
      }}
    >
      {children}
    </Carousel>
  );
}

CarouselWrapper.propTypes = {
  children: PropTypes.node.isRequired,
};

const CarouselButton = styled.button`
  background: none;
  border: 0;
  bottom: 0;
  color: #000000;
  cursor: pointer;
  font-size: 26px;
  margin: 0;
  padding: 0;
  position: absolute;
  opacity: 1;
  outline: 0;
  top: 0;
  transition: all 0.25s ease-in;
  z-index: 2;
`;

export default CarouselWrapper;
