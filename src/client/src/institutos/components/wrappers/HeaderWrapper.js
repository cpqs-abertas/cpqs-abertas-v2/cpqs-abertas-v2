import React, { useContext } from "react";
import Header from "../../../common/misc/Header";
import PropTypes from "prop-types";
import InstituteStylesContext from "../../context/InstituteStyles";

function HeaderWrapper(props) {
  const instituteStyle = useContext(InstituteStylesContext);
  const { setMenuFilter, menuFilter } = props;

  return (
    <Header
      title={instituteStyle.titulo}
      titleColor={instituteStyle.titulo_Cor}
      subtitle={instituteStyle.nome}
      subtitleColor={instituteStyle.subTitleColor}
      fontFamily={instituteStyle.fontFamily}
      instituteLogo={instituteStyle.logo}
      setMenuFilter={setMenuFilter}
      menuFilter={menuFilter}
    />
  );
}

HeaderWrapper.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  titleColor: PropTypes.string.isRequired,
  subTitleColor: PropTypes.string.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
  menuFilter: PropTypes.object.isRequired,
};

HeaderWrapper.defaultProps = {
  titleColor: "",
  title: "",
  subtitle: "",
  subTitleColor: "",
};

export default HeaderWrapper;
