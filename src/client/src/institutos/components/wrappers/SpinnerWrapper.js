import React from "react";
import SolFAU from "../../assets/images/FAUUSP-logo.png";
import Spinner from "../../../common/misc/Spinner";

function SpinnerWrapper() {
  return <Spinner image={SolFAU} width={100} height={100} />;
}

export default SpinnerWrapper;
