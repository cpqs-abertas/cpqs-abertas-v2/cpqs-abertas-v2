import { React, useContext } from "react";
import InstituteStylesContext from "../../context/InstituteStyles";
import Fader from "../../../common/misc/Fader";

function FaderWrapper() {
  const instituteStyle = useContext(InstituteStylesContext);

  return <Fader image={instituteStyle.logo} />;
}

export default FaderWrapper;
