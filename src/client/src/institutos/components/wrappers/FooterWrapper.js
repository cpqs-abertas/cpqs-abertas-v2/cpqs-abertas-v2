import React, { useContext } from "react";
import styled from "styled-components";
import Footer from "../../../common/misc/Footer";
import InstituteStylesContext from "../../context/InstituteStyles";

function FooterWrapper() {
  const instituteStyles = useContext(InstituteStylesContext);

  return (
    <Footer font={instituteStyles.fontFamily}>
      {Object.keys(instituteStyles.Footer).map((key) => {
        return (
          <Info>
            <div>{instituteStyles.Footer[key].name}</div>
            <div> {instituteStyles.Footer[key].adr} </div>
            {instituteStyles.Footer[key].mail && (
              <div>
                {"contato: "}
                <a
                  style={{ textDecoration: "none", color: "#000" }}
                  href={`mailto:${instituteStyles.Footer[key].mail}`}
                >
                  {instituteStyles.Footer[key].mail}
                </a>{" "}
              </div>
            )}
            <div>{instituteStyles.Footer[key].tel}</div>
          </Info>
        );
      })}
    </Footer>
  );
}

const Info = styled.div`
  align-items: left;
  color: black;
  display: flex;
  flex-direction: column;

  > div {
    font-size: 12px;
    margin-bottom: 4px;
    text-align: right;
    padding-right: 72px;
  }

  > a {
    color: white;
    font-size: 20px;
    text-decoration: none;
  }
`;

export default FooterWrapper;
