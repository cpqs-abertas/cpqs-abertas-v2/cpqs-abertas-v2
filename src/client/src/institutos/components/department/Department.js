/* eslint-disable */

import React, { useEffect, useContext } from "react";
import PropTypes from "prop-types";
import InstituteStylesContext from "../../context/InstituteStyles";
import {
  getDashboardCount,
  getDashboardCountByDep,
  getDashboard,
  getDashboardByDep,
  getDashboardKeywords,
  getDashboardMap,
  getDashboardNationalMap,
} from "../../../common/API";
import {
  Conteudo,
  InfoText,
  Director_Vice,
  Title,
  DivGraph,
  DivInfoText,
  DivInfoProf,
  BarChartWrapper,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
} from "../../styles";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import BarChartContainer from "../../../common/charts/BarChartContainer";
import LineChartContainer from "../../../common/charts/LineChartContainer";
import NationalMapContainer from "../../../common/maps/NationalMapContainer";
import WordCloudContainer from "../../../common/keywords/WordCloudContainer";
import WorldMapContainer from "../../../common/maps/WorldMapContainer";
import FaderWrapper from "../wrappers/FaderWrapper";
import CarouselWrapper from "../wrappers/CarouselWrapper";
import { setFilters, createCountByYearBySinlgeDep } from "../../helpers";

function Department(props) {
  const {
    setMenuFilter,
    dep,
    depDescription,
    chefe,
    viceChefe,
    children,
    color,
  } = props;

  const instituteStyle = useContext(InstituteStylesContext);

  const barColors = [
    instituteStyle.graphs.barChart.color1,
    instituteStyle.graphs.barChart.color2,
    instituteStyle.graphs.barChart.color3,
    instituteStyle.graphs.barChart.color4,
    instituteStyle.graphs.barChart.color5,
    instituteStyle.graphs.barChart.color6,
    instituteStyle.graphs.barChart.color7,
    instituteStyle.graphs.barChart.color8,
  ];

  const nuvemColors = [
    instituteStyle.graphs.worldCloud.color1,
    instituteStyle.graphs.worldCloud.color2,
    instituteStyle.graphs.worldCloud.color3,
    instituteStyle.graphs.worldCloud.color4,
  ];

  useEffect(() => {
    if (dep) {
      setFilters(setMenuFilter, { [dep]: true }, (filter, newState) => {
        newState[filter] = false;
      });
    }
  }, [dep]);

  return (
    <Conteudo>
      <div id="left">
        <InfoText>
          <Title>{depDescription}</Title>
          <Director_Vice id="dep_director_vice">
            <span id="chefe">Chefe do departamento
              <p>{chefe}</p>
            </span>
            <span id="vice_chefe">Vice-chefe do departamento
              <p>{viceChefe}</p>
            </span>
          </Director_Vice>
        </InfoText>
        <DivGraph>
          <CarouselWrapper>
            <WordCloudWrapper>
              <WordCloudContainer
                colors={nuvemColors}
                fetchData={() =>
                  getDashboardKeywords({ limit: 50, departamento: dep })
                }
                Loading={FaderWrapper}
              />
            </WordCloudWrapper>
            <WorldMapWrapper>
              <WorldMapContainer
                fetchData={() => getDashboardMap({ departamento: dep })}
                color={instituteStyle.graphs.map.color}
              />
            </WorldMapWrapper>
            <NationalMapWrapper>
              <NationalMapContainer
                fetchData={() => getDashboardNationalMap({ departamento: dep })}
                color={instituteStyle.graphs.map.color}
              />
            </NationalMapWrapper>
            <BarChartWrapper>
              <BarChartContainer
                labels={[instituteStyle.graphs.barChart.instituteLabel, dep]}
                colors={barColors}
                fetchData={async () => {
                  const FAUCount = await getDashboard();
                  const depCount = await getDashboardByDep({
                    departamento: dep,
                  });
                  return {
                    countByYearByCategoryByLabel: {
                      [instituteStyle.graphs.barChart.instituteLabel.replace(
                        /[""]/g,
                        ""
                      )]: FAUCount,
                      [dep]: depCount,
                    },
                    categories: [
                      "Produção Artística",
                      "Produção Técnica",
                      "Produção Bibliográfica",
                      "Orientação",
                      "Bancas",
                      "Prêmios e Títulos",
                    ],
                  };
                }}
              />
            </BarChartWrapper>
            <LineChartWrapper>
              <LineChartContainer
                labels={[dep, instituteStyle.nome]}
                color={{
                  line1: color,
                  line2: instituteStyle.graphs.lineChart.color,
                }}
                fill={true}
                formatData={createCountByYearBySinlgeDep}
                fetchData={async () => {
                  const instCountByYear = await getDashboardCount({
                    ano_inicio: 1978,
                  });
                  const countByYearByDep = await getDashboardCountByDep({
                    ano_inicio: 1978,
                    departamento: dep,
                  });
                  const aux = { dep: countByYearByDep[dep] };
                  return {
                    line1: aux,
                    line2: instCountByYear,
                  };
                }}
                Loading={FaderWrapper}
              />
            </LineChartWrapper>
          </CarouselWrapper>
        </DivGraph>
      </div>
      <div id="right">
        <DivInfoText className="infotext">
          <span className="infotext_title">Histórico</span>
          <span className="infotext_content">{children}</span>
        </DivInfoText>
      </div>
    </Conteudo>
  );
}

Department.propTypes = {
  chefe: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  dep: PropTypes.string.isRequired,
  depDescription: PropTypes.string.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
  viceChefe: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  borderColor: PropTypes.string.isRequired,
  borderWidth: PropTypes.number.isRequired,
};

export default Department;
