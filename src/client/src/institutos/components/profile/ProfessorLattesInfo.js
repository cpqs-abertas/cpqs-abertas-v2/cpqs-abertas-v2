/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { departmentColors, DivInfo, DivInfoProf } from "../../styles";
import PhotoWrapper from "../wrappers/PhotoWrapper";

function ProfessorLattesInfo(props) {
  const { lattesId, fullName, contactNumber, menuFilter, departament } = props;

  return (
    <DivInfo>
      <div id="superior">        
        <span id="full_name">{fullName}</span>
      </div>
      <div id="inferior">
        <PhotoWrapper id={lattesId} menuFilter={menuFilter}/>
        <DivInfoProf color={departmentColors[departament]}>
          <span id="phone">Telefone</span>
          {contactNumber ? <span id="phone_number">{contactNumber}</span> : <span>-</span>}
          <span id="curriculum">Currículo Lattes</span>
          <a id="curriculum_link"
            href={`http://lattes.cnpq.br/${lattesId}`}
            target="_blank"
            rel="noreferrer"
          >
            http://lattes.cnpq.br/
            <br/>
            {lattesId}
          </a>
        </DivInfoProf>
      </div>
    </DivInfo>
  );
}

ProfessorLattesInfo.propTypes = {
  contactNumber: PropTypes.string.isRequired,
  fullName: PropTypes.string.isRequired,
  lattesId: PropTypes.string.isRequired,
  departament: PropTypes.string.isRequired,
  menuFilter: PropTypes.object.isRequired,
};

export default ProfessorLattesInfo;
