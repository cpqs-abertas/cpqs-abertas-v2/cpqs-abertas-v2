const setFilters = (setState, filtersToIsActiveMap, handleFilter) => {
  setState((prevState) => {
    const newState = {};
    Object.keys(prevState).forEach((filter) => {
      handleFilter(filter, newState, prevState);
    });

    Object.keys(filtersToIsActiveMap).forEach((filter) => {
      newState[filter] = filtersToIsActiveMap[filter];
    });
    return newState;
  });
};

const createCountByCategoryByLabel = (countByYearByCategoryByLabel) => {
  /* Example
    - params:
      - countByYearByCategoryByLabel = {
        label1: {
          category1: {
            ...
            1990: 10,
            1991: 3,
            ...
          },
          ...
        },
        ...
      }
    - return value = {
        label1: {
          category1: 13,
          ...
        },
        ...
      }
  */

  const countByCategoryByLabel = {};
  Object.keys(countByYearByCategoryByLabel).forEach((label) => {
    countByCategoryByLabel[label] = {};
    Object.keys(countByYearByCategoryByLabel[label]).forEach((category) => {
      const countByYear = countByYearByCategoryByLabel[label][category];
      countByCategoryByLabel[label][category] = Object.values(
        countByYear
      ).reduce((total, current) => total + current);
    });
  });

  return countByCategoryByLabel;
};

const createCountByYear = (countByYearByProductionType) => {
  /*  Example:
      - params:
        - countByYearByProductionType = {
          productionType1: {
            ...
            1990: 10,
            1991: 3,
            ...
          },
          productionType2: {
            ...
            1990: 7,
            1991: 7,
            ...
          },
        }

      - return value = {
          ...
          1990: 17,
          1991: 10,
          ...
        }
  */

  const data = {};

  Object.values(countByYearByProductionType).forEach((countByYear) => {
    Object.keys(countByYear).forEach((year) => {
      data[year] = (data[year] || 0) + countByYear[year];
    });
  });

  return data;
};

const createCountByYearBySinlgeDep = (productionTypeYearCountByDep) => {
  /*  Example:
      - params
        - productionTypeYearCountByDep = {
            department1: {
              productionType1: {
                1990: 10,
                1991: 3,
              },
              productionType2: {
                1990: 7,
                1991: 7,
              },
            },
            ...
          }

      - return value = {
          department1: {
            1990: 17,
            1991: 10,
          },
          ...
        }
  */
  let countByYearByDep = {};
  // eslint-disable-next-line no-unused-vars
  var numDep;

  Object.values(productionTypeYearCountByDep).forEach((dep, index) => {
    numDep++;
    index === 0 && (countByYearByDep["line1"] = {});
    index === 1 && (countByYearByDep["line2"] = {});
    index === 2 && (countByYearByDep["line3"] = {});
    index === 3 && (countByYearByDep["line4"] = {});
    index === 4 && (countByYearByDep["line5"] = {});
    index === 5 && (countByYearByDep["line6"] = {});
    index === 6 && (countByYearByDep["line7"] = {});
  });

  let line = 1;
  Object.values(productionTypeYearCountByDep).forEach((dep) => {
    Object.values(dep).forEach((countByYear) => {
      Object.keys(countByYear).forEach((year) => {
        const count = countByYear[year];
        line === 1 &&
          (countByYearByDep.line1[year] =
            (countByYearByDep.line1[year] || 0) + count);

        line === 2 &&
          (countByYearByDep.line2[year] =
            (countByYearByDep.line2[year] || 0) + count);

        line === 3 &&
          (countByYearByDep.line3[year] =
            (countByYearByDep.line3[year] || 0) + count);

        line === 4 &&
          (countByYearByDep.line4[year] =
            (countByYearByDep.line4[year] || 0) + count);

        line === 5 &&
          (countByYearByDep.line5[year] =
            (countByYearByDep.line5[year] || 0) + count);

        line === 6 &&
          (countByYearByDep.line6[year] =
            (countByYearByDep.line6[year] || 0) + count);

        line === 7 &&
          (countByYearByDep.line7[year] =
            (countByYearByDep.line7[year] || 0) + count);
      });
    });
    line++;
  });
  //           countByYearByDep[dep][year] = (countByYearByDep[dep][year] || 0) + count;

  // departments.forEach((dep) => {
  //   if (productionTypeYearCountByDep[dep]) {
  //     countByYearByDep[dep] = {};
  //     Object.values(productionTypeYearCountByDep[dep]).forEach((countByYear) => {
  //         Object.keys(countByYear).forEach((year) => {
  //           const count = countByYear[year];
  //           countByYearByDep[dep][year] = (countByYearByDep[dep][year] || 0) + count;
  //         });
  //       }
  //     );
  //   }
  // });

  return countByYearByDep;
};

const createCountByYearByDep = (productionTypeYearCountByDep) => {
  /*  Example:
      - params
        - productionTypeYearCountByDep = {
            department1: {
              productionType1: {
                1990: 10,
                1991: 3,
              },
              productionType2: {
                1990: 7,
                1991: 7,
              },
            },
            ...
          }

      - return value = {
          department1: {
            1990: 17,
            1991: 10,
          },
          ...
        }
  */
  let countByYearByDep = {};
  // eslint-disable-next-line no-unused-vars
  var numDep = 0;

  Object.values(productionTypeYearCountByDep).forEach((dep, index) => {
    numDep++;
    index === 0 && (countByYearByDep["line1"] = {});
    index === 1 && (countByYearByDep["line2"] = {});
    index === 2 && (countByYearByDep["line3"] = {});
    index === 3 && (countByYearByDep["line4"] = {});
    index === 4 && (countByYearByDep["line5"] = {});
    index === 5 && (countByYearByDep["line6"] = {});
    index === 6 && (countByYearByDep["line7"] = {});
  });

  let line = 1;
  Object.values(productionTypeYearCountByDep).forEach((dep) => {
    Object.values(dep).forEach((countByYear) => {
      Object.keys(countByYear).forEach((year) => {
        const count = countByYear[year];
        line === 1 &&
          (countByYearByDep.line1[year] =
            (countByYearByDep.line1[year] || 0) + count);

        line === 2 &&
          (countByYearByDep.line2[year] =
            (countByYearByDep.line2[year] || 0) + count);

        line === 3 &&
          (countByYearByDep.line3[year] =
            (countByYearByDep.line3[year] || 0) + count);

        line === 4 &&
          (countByYearByDep.line4[year] =
            (countByYearByDep.line4[year] || 0) + count);

        line === 5 &&
          (countByYearByDep.line5[year] =
            (countByYearByDep.line5[year] || 0) + count);

        line === 6 &&
          (countByYearByDep.line6[year] =
            (countByYearByDep.line6[year] || 0) + count);

        line === 7 &&
          (countByYearByDep.line7[year] =
            (countByYearByDep.line7[year] || 0) + count);
      });
    });
    line++;
  });
  //           countByYearByDep[dep][year] = (countByYearByDep[dep][year] || 0) + count;

  // departments.forEach((dep) => {
  //   if (productionTypeYearCountByDep[dep]) {
  //     countByYearByDep[dep] = {};
  //     Object.values(productionTypeYearCountByDep[dep]).forEach((countByYear) => {
  //         Object.keys(countByYear).forEach((year) => {
  //           const count = countByYear[year];
  //           countByYearByDep[dep][year] = (countByYearByDep[dep][year] || 0) + count;
  //         });
  //       }
  //     );
  //   }
  // });

  return countByYearByDep;
};

const createInstituteCountByYear = (countByYearByProductionTypeByDep) => {
  /*  Example:
      - params
        - countByYearByProductionTypeByDep = {
            department1: {
              productionType1: {
                1990: 10,
                1991: 3,
              },
              productionType2: {
                1990: 7,
                1991: 7,
              },
            },
            department2: {
              productionType1: {
                1990: 4,
                1991: 5,
              },
              productionType2: {
                1990: 6,
                1991: 6,
              },
            },
          }

      - return value = {
          FAU: {
            1990: 27,
            1991: 21,
          },
        }
  */

  const InsCountByYear = { line1: {} };

  Object.values(countByYearByProductionTypeByDep).forEach((dep) => {
    if (typeof Object.values(dep)[0] === "number") {
      Object.keys(dep).forEach((year) => {
        const count = dep[year];
        InsCountByYear.line1[year] = (InsCountByYear.line1[year] || 0) + count;
      });
    } else {
      Object.values(dep).forEach((countByYear) => {
        Object.keys(countByYear).forEach((year) => {
          const count = countByYear[year];
          InsCountByYear.line1[year] =
            (InsCountByYear.line1[year] || 0) + count;
        });
      });
    }
  });
  return InsCountByYear;
};

export default setFilters;
export {
  setFilters,
  createCountByCategoryByLabel,
  createCountByYear,
  createCountByYearByDep,
  createInstituteCountByYear,
  createCountByYearBySinlgeDep,
};
