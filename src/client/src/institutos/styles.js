/* eslint-disable */

import styled from "styled-components";

const fontFamily = "sans-serif";

const instituteColor = "#000000";


/* Apagar... */
const departmentColors = {
  /* DCC: "yellow",
  MAE: "black",
  MAT: "black",
  MAP: "black",
  DOCENTE: "#red", */
};

/* Apagar... */
const wordCloudColors = {
  /* AUH: ["#6d6d6d", "#585859", "#353535", "#000"],
  AUP: ["#6d6d6d", "#585859", "#353535", "#000"],
  AUT: ["#6d6d6d", "#585859", "#353535", "#000"],
  default: ["#6d6d6d", "#585859", "#353535", "#000"], */
};

/* Apagar... */
const worldMapColors = {
  /*AUH: "#000000",
  AUP: "#000000",
  AUT: "#000000", */
};

/* STYLE BODY */
const Body = styled.div`
  display: grid;
  font-family: ${fontFamily};
  grid-template-columns: 1fr 340px 1100px 1fr;
  grid-template-rows: 212px 690px 136px;

  /* Apagar depois 
  & > * {
    border: 1px solid #000; 
  } */

  button,
  datalist,
  fieldset,
  input,
  label,
  legend,
  output,
  option,
  optgroup,
  select,
  textarea {
    font-family: ${fontFamily};
  }

  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
`;

const BannerStyle = styled.div`
  display: flex;
  width: 100%;
  background-color: #FCB421;
  justify-content: center;
`;

/* STYLE CONTEÚDO HOME */
const Conteudo = styled.div`
  display: grid;
  grid-column-start: 3;
  grid-row-start: 2;
  grid-template-columns: 1fr 1fr;
  # background-color: lightgray;
  height: 690px;
  width: 1025px;
  
  /* 
  & > * {
    border: 1px solid #000; 
  } */

  > div {
    # background-color: lightgreen;
    height: 690px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: 512.5px;
  }

  #homepage {
    # background-color: purple;
    height: 320px;
  }

  #department_page {
    # background-color: lightblue;
    height: 690px;
  }

  #right {
    # background-color: lightblue;
  }

  #left {
    # background-color: lightgreen;
  }
`;

/* STYLE INFOS INSTITUTO/DEPARTAMENTO */
const InfoText = styled.div`
  font-family: ${(props) => props.fontFamily};
  height: 243px;
  width: 512.5px;
  # background-color: blue;

  > a {
    text-decoration: none !important;
  }

  > div {
    display: flex;
    flex-direction: row;
    width: 490px;
    margin-top: 24px;
  }

  #dep_director_vice {
    display: flex;
    flex-direction: column;
  }
`;

const Title = styled.div`
  color: ${(props) => props.color};
  font-size: 25px;
  font-weight: bold;
  text-align: left;
  width: 490px;
  margin-top: 24px;
  # background-color: green;
`;

const Director_Vice = styled.div`
  font-weight: bold;
  # background-color: yellow;
  font-size: 20px;

  > span { 
    text-align: left;
    font-weight: normal;
  }

  > span > p {
    margin: 0;
    font-weight: bold;
  }

  #diretor {
    padding-right: 10px;
  }

  #vice {
    padding-left: 10px;
  }

  #vice_chefe {
    margin-top: 24px;
  }
`

const Institute_Site = styled.div`
  # background-color: pink;
  font-size: 20px;
  > span > p {
    margin: 0;
    font-weight: bold;
    text-align: left;
  }

  > span > p > a {
    color: black;
    text-decoration: none;
  }
`;

/* STYLE RESULTADOS */
const DivTotal = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: flex-end;
  text-align: left;
  # background-color: lightblue;
  padding-bottom: 72px;

  > span {
    font-size: 16px;
    font-weight: normal;
  }

  #totalProd {
    # background-color: pink;
    font-size: 136px;
    font-weight: bold;
    margin-bottom: 8px;
  }

  #result {
    font-size: 20px;
    # background-color: lightyellow;
    font-weight: bold;
    margin-bottom: 16px;
  }

  #lastUpdate {
    font-size: 16px;
    font-weight: normal;
    # background-color: lightgreen;
  }
`;

/* STYLE TEXTO INSTITUCIONAL, HISTÓRICO DEPARTAMENTO, APRESENTAÇÃO PRODUÇÃO & RESUMO ACADÊMICO */
const DivInfoText = styled.div`
  display: flex;
  flex-flow: column;
  margin-left: 15px;
  text-align: left;
  font-size: 20px;
  text-align: justify;
  # background-color: purple;

  .infotext {
    margin-top: 24px;
  }

  .infotext_title {
    margin-top: 24px;
    margin-bottom: 16px;
    font-weight: bold;
    font-size: 25px;
    # background-color: lightblue;
  }

  .infotext_content {
    display: flex;
    flex-flow: column;
    height: 620px;
    width: 100%;
    overflow-x: hidden;
    # background-color: lightgreen;
    scrollbar-width: thin;
  }

  #infotext_prod {
    display: flex;
    flex-direction: column;
    # background-color: brown;
  }
`;

/* STYLE GRÁFICOS */
const DivGraph = styled.div`
  # background-color: purple;
  grid-row-start: 2;
  grid-column-start: 2;
  height: 340px;
  width: 512.5px;

  .carousel.carousel-slider,
  .carousel-root {
    height: 100%;
  }

  .carousel .slide {
    background: #fff;
  }

  .carousel .control-arrow:before,
  .carousel.carousel-slider .control-arrow:before {
    border-top: 12px solid transparent;
    border-bottom: 12px solid transparent;
  }

  .carousel .control-next.control-arrow:before {
    border-left: 15px solid #000;
  }

  .carousel .control-prev.control-arrow:before {
    border-right: 15px solid #000;
  }

  .carousel .control-arrow,
  .carousel.carousel-slider .control-arrow {
    opacity: 1;
    margin-left: -10px;
    margin-right: -10px;
  }

  .carousel.carousel-slider .control-arrow:hover {
    background: rgba(0, 0, 0, 0);
  }
`;

const BarChartWrapper = styled.div`
  background-color: white;
  padding: 20px 0 20px 40px;
`;

const LineChartWrapper = styled.div`
  background-color: white;
  padding: 20px 0 20px 40px;

  .rc-slider-track {
    background-color: #000000;
  }

  .rc-slider-handle {
    background-color: #000000;
    border-color: #000000;
  }

  .rc-slider-handle:hover {
    border-color: #000000;
  }

  .rc-slider-handle:focus {
    border-color: #000000;
  }

  .rc-slider-handle:active {
    border-color: #000000;
    box-shadow: 0 0 5px #000000;
  }

  .rc-slider-rail {
    background-color: #cccccc;
  }
`;

const NationalMapWrapper = styled.div`
  background-color: white;
`;

const WordCloudWrapper = styled.div`
  background-color: white;
  height: 100%;
  padding: 0 0 10px 10px;
`;

const WorldMapWrapper = styled.div`
  background-color: white;
`;


/* STYLE INFOS DOCENTE */
const DivInfo = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: space-between;
  # background-color: lightblue;
  margin-top: 24px;
  height: 225px;

  #superior {
    # background-color: lightyellow;
    height: 60px;
    font-size: 25px;
    font-weight: bold;
  }

  #inferior {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    # background-color: pink;
    height: 150px;
  }
`;

const DivInfoProf = styled.div`
  display: flex;
  flex-flow: column;
  # background-color: white;
  padding-left: 30px;
  width: 300px;
  font-size: 20px;

  #phone_number {
    font-weight: bold;
    padding-bottom: 20px;
  }

  #curriculum_link {
    font-decoration: none;
    font-weight: bold;
    color: black;
  }
`;

const ProfPhoto = styled.div`
  display: flex;
  align-items: center;
  height: 150px;
  width: 160px;
  # background-color: magenta;
} 
`

/* STYLE PRODUÇÕES DOCENTES */

/* Não sei para que servem ainda... */

const DivLink = styled.div`
  # background-color: lightblue;
  height: 690px;

  > div {
  display:flex
  flex-direction: column;
  flex-overflow: column;
  # background-color: yellow;

  > span#ano {
    font-size: 24px;
    font-weight: bold;
    # background: magenta;
  }
`;

/* STYLE PARA LISTA DOS DOCENTES */
const CenteredColumn = styled.div`
  margin-top: 16px;
  margin-left: -8px;
  align-items: center;
  display: flex;
  flex-direction: column;
  grid-column-start: 3;
  grid-row-start: 2;
  overflow-y: auto;
  justify-content: center;
  width: 1025px;
`;


const DivLinkDocentes = styled.div`
  background-color: #fff;
  display: flex;
  flex-wrap: wrap;

  > span {
    display: flex;
    font-size: 16px;
    font-weight: bold;
    opacity: 1;
    height: 19px;
    margin: 8px 8px 8px 8px;
    # background-color: pink;


    a {
      color: #000;
      align-itens: center;
      justify-content: center;
      font-size: 16px;
      font-weight: bold;
      margin-bottom: 3.7px;
      opacity: 0.8;
      text-decoration: none;
    }
  }
`;

const DivDetailBlock = styled.div`
  a {
    color: ${(props) => (props.color ? props.color : "#000")};
    font-size: 15px;
    font-weight: bold;
    text-decoration: none;
  }
`;

/* Talvez apagar... */
const DivCard = styled.div`
  display: grid;
  height: 690px;
  width: 525px;
  background-color: green;
  
  /*
  & > * {
    border: 1px solid #000; 
    text-align: center;
  } */
`;

/* Talvez apgar... */
const DivInfoProducao = styled.div`
  display: grid;
  grid-row-start: 2;
  grid-column-start: 1;
  # background-color: yellow;
  width: 550px;
  height: 345px;
`;

/* Talvez apagar... */

/* FOOTER */
const Container = styled.div`
  grid-column-start: 3;
  grid-row-start: 3;
  width: 100%;
  # background-color: yellow;
  width: 1440px;
  height: 136px;
  padding: 0 72px 0 72px;
  display: flex;
  align-items: center;
`;

const ContainerBottom = styled.div`
  display: flex;
  overflow: hidden;
  padding-top: 30px;
  width: 100%;
  justify-content: space-between;
  > a {
    font-weight: 700;
  }
`;

const HubLink = styled.div`
  color: black;
  padding-left: 72px;
  font-weight: bold;

  > a {
    text-decoration: underline;
  }
`;

export {
  departmentColors,
  wordCloudColors,
  worldMapColors,
  instituteColor,
  BarChartWrapper,
  Body,
  CenteredColumn,
  Conteudo,
  DivCard,
  DivDetailBlock,
  DivGraph,
  DivInfo,
  InfoText,
  Title,
  Director_Vice,
  Institute_Site,
  DivInfoProducao,
  DivInfoProf,
  DivInfoText,
  DivLink,
  DivLinkDocentes,
  DivTotal,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
  Container,
  ContainerBottom,
  HubLink,
  ProfPhoto,
  BannerStyle,
};
