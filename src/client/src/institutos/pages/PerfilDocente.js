/*eslint-disable*/
import React, { useState, useEffect, useContext, StrictMode } from "react";
import PropTypes from "prop-types";
import { useParams } from "react-router-dom";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import InstituteStylesContext from "../context/InstituteStyles";
import {
  Conteudo,
  DivGraph,
  DivInfoText,
  departmentColors,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
} from "../styles";
import {
  getPessoa,
  getKeywordsPessoa,
  getMapPessoa,
  getMapPessoaNational,
  getCountAllPessoa,
  getDashboardCountByDep,
} from "../../common/API";
import ProfessorLattesInfo from "../components/profile/ProfessorLattesInfo";
import LineChartContainer from "../../common/charts/LineChartContainer";
import NationalMapContainer from "../../common/maps/NationalMapContainer";
import WordCloudContainer from "../../common/keywords/WordCloudContainer";
import WorldMapContainer from "../../common/maps/WorldMapContainer";
import FaderWrapper from "../components/wrappers/FaderWrapper";
import { setFilters } from "../helpers";
import CarouselWrapper from "../components/wrappers/CarouselWrapper";

function PerfilDocente(props) {
  const { menuFilter, setMenuFilter } = props;
  const { id } = useParams();
  const [data, setData] = useState(null);

  useEffect(() => {
    getPessoa({ id }).then((responsePayload) => {
      setData(responsePayload);

      const dep = responsePayload.departamento;

      setFilters(
        setMenuFilter,
        {
          docentes: true,
          perfil: true,
          [dep]: true,
        },
        (filter, newState) => {
          newState[filter] = false;
        }
      );
    });
  }, []);

  if (!data) {
    return (
      <Conteudo
        style={{
          alignItems: "center",
          border: "0",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <FaderWrapper />
      </Conteudo>
    );
  }

  return <Perfil data={data} id={id} menuFilter={menuFilter} />;
}

function Perfil(props) {
  const { data, id, menuFilter } = props;

  if (!data.departamento) {
    return (
      <Conteudo style={{ justifyContent: "start", width: 1100 }}>
        Não foi possível encontrar essa pessoa.
      </Conteudo>
    );
  }

  const instituteStyle = useContext(InstituteStylesContext);
  const wordCloudColorsDefault = [
    instituteStyle.graphs.worldCloud.color1,
    instituteStyle.graphs.worldCloud.color2,
    instituteStyle.graphs.worldCloud.color3,
    instituteStyle.graphs.worldCloud.color4,
  ];
  return (
    <Conteudo>
      <div id="left">
        <ProfessorLattesInfo
          lattesId={data.id_lattes}
          contactNumber={data.contato}
          fullName={data.nome_completo}
          menuFilter={menuFilter}
          departament={data.departamento}
        />
        <DivGraph>
          <CarouselWrapper>
            <WordCloudWrapper>
              <WordCloudContainer
                colors={wordCloudColorsDefault}
                fetchData={() => getKeywordsPessoa({ id, limit: 50 })}
                Loading={FaderWrapper}
              />
            </WordCloudWrapper>
            <WorldMapWrapper>
              <WorldMapContainer
                fetchData={() => getMapPessoa({ id })}
                color={instituteStyle.graphs.map.color}
                Loading={FaderWrapper}
              />
            </WorldMapWrapper>
            <NationalMapWrapper>
              <NationalMapContainer
                fetchData={() => getMapPessoaNational({ id })}
                color={instituteStyle.graphs.map.color}
                Loading={FaderWrapper}
              />
            </NationalMapWrapper>
            <StrictMode>
              <LineChartWrapper>
                <LineChartContainer
                  labels={["DOCENTE", data.departamento]}
                  color={{
                    DOCENTE: departmentColors.DOCENTE,
                    [data.departamento]: departmentColors[data.departamento],
                  }}
                  fill={{
                    DOCENTE: "origin",
                    [data.departamento]: "-1",
                  }}
                  fetchData={async () => {
                    const professor = await getPessoa({ id });
                    const dep = professor.departamento;
                    const countByYearByDep = await getDashboardCountByDep({
                      ano_inicio: 1978,
                      departamento: dep,
                    });
                    const professorCountByYear = await getCountAllPessoa({ id });
                    console.log("conta_por_ano", JSON.stringify(countByYearByDep));
                    return {
                      DOCENTE: professorCountByYear.DOCENTE,
                      [dep]: countByYearByDep[dep],
                    };
                  }}
                  Loading={FaderWrapper}
                />
              </LineChartWrapper>
            </StrictMode>
          </CarouselWrapper>
        </DivGraph>
      </div>
      <div id="right">
        <DivInfoText>
          <span className="infotext_title">Resumo Acadêmico</span>
          <div className="infotext_content">{data.resumo}</div>
        </DivInfoText>
      </div>
    </Conteudo>
  );
}

PerfilDocente.propTypes = {
  menuFilter: PropTypes.object.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

Perfil.propTypes = {
  data: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  menuFilter: PropTypes.object.isRequired,
};

export default PerfilDocente;
