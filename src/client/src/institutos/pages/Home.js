/* eslint-disable */
import React, { useEffect, useState, useContext } from "react";
import PropTypes from "prop-types";
import InstituteStylesContext from "../context/InstituteStyles";
import { Link } from "react-router-dom";
import {
  getDashboardAggregatedCount,
  getDashboardAggregated,
  getDashboardAggregatedMap,
  getDashboardAggregatedNationalMap,
  getLastUpdateDate,
} from "../../common/API";
import {
  Conteudo,
  DivGraph,
  DivInfoText,
  InfoText,
  Title,
  Director_Vice,
  Institute_Site,
  DivTotal,
  BarChartWrapper,
  LineChartWrapper,
  NationalMapWrapper,
  WorldMapWrapper,
} from "../styles";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import BarChartContainer from "../../common/charts/BarChartContainer";
import LineChartContainer from "../../common/charts/LineChartContainer";
import NationalMapContainer from "../../common/maps/NationalMapContainer";
import WorldMapContainer from "../../common/maps/WorldMapContainer";
import FaderWrapper from "../components/wrappers/FaderWrapper";
import CarouselWrapper from "../components/wrappers/CarouselWrapper";
import { setFilters, createInstituteCountByYear } from "../helpers";

function Home(props) {
  const { setMenuFilter } = props;
  const [countByProductionType, setCountByProductionType] = useState(null);
  const [lastDataUpdateDate, setLastDataUpdateDate] = useState(null);
  const instituteStyle = useContext(InstituteStylesContext);

  useEffect(() => {
    getDashboardAggregated().then((responsePayload) => {
      setCountByProductionType(responsePayload);
    });

    setFilters(setMenuFilter, { unidade: true }, (filter, newState) => {
      newState[filter] = false;
    });
  }, []);

  useEffect(() => {
    getLastUpdateDate().then((res) => {
      setLastDataUpdateDate(res);
    });
  }, []);

  if (!countByProductionType) {
    return (
      <Conteudo>
        <FaderWrapper />
      </Conteudo>
    );
  }

  return (
    <HomeData
      countByProductionType={countByProductionType}
      title={instituteStyle.titulo}
      titleColor={instituteStyle.titulo_Cor}
      subtitle={instituteStyle.nome}
      subtitleColor={instituteStyle.subtitleColor}
      fontFamily={instituteStyle.fontFamily}
      lastDataUpdateDate={lastDataUpdateDate}
      font={instituteStyle.fontFamily}
      acronym={instituteStyle.acronym}
      director={instituteStyle.diretor}
      vice_director={instituteStyle.vicediretor}
      officialsite={instituteStyle.officialsite}
    />
  );
}

function HomeData(props) {
  const {
    countByProductionType,
    title,
    subtitle,
    titleColor,
    fontFamily,
    lastDataUpdateDate,
    acronym,
    director,
    vice_director,
    officialsite,
  } = props;
  const total = Object.values(countByProductionType).reduce(
    (accumulator, value) => accumulator + value,
    0
  );
  const instituteStyle = useContext(InstituteStylesContext);
  const barColors = [
    instituteStyle.graphs.barChart.color1,
    instituteStyle.graphs.barChart.color2,
    instituteStyle.graphs.barChart.color3,
    instituteStyle.graphs.barChart.color4,
    instituteStyle.graphs.barChart.color5,
    instituteStyle.graphs.barChart.color6,
    instituteStyle.graphs.barChart.color7,
    instituteStyle.graphs.barChart.color8,
  ];

  return (
    <Conteudo>
      <div id="left">
        <InfoText>
          <Link to="/">
            <Title color={titleColor}>{title} - {acronym} USP </Title>
          </Link>
          <Director_Vice>
            <span id="diretor">
              Diretor:
              <p>{director}</p>
            </span>
            <span id="vice">
              Vice-diretor:
              <p>{vice_director}</p>
            </span>
          </Director_Vice>
          <Institute_Site>
            <span> 
              Site Oficial:
              <p>
                <a href={officialsite} target="_blank">{officialsite}</a>
              </p>

            </span>
          </Institute_Site>
        </InfoText>
        <DivTotal>
          <span id="totalProd" style={{color: `${instituteStyle.titulo_Cor}`,}}>
            {Intl.NumberFormat().format(total)}
          </span>
          <span id="result">resultados</span>
          <span>Retirados da plataforma Lattes.</span>
          <span id="lastUpdate">Última atualização em: {lastDataUpdateDate || "Não há registro."}</span>
        </DivTotal>
      </div>
      <div id="right">
        <DivInfoText id="homepage">
          <div className="infotext_content"
            dangerouslySetInnerHTML={{
              __html: instituteStyle.homeDescription,
            }}
          >
          </div>
        </DivInfoText>
        <DivGraph>
          <CarouselWrapper>
            <LineChartWrapper>
              <LineChartContainer
                labels={[instituteStyle.nome]}
                formatData={createInstituteCountByYear}
                color={{ line1: instituteStyle.graphs.lineChart.color }}
                fill={true}
                fetchData={() =>
                  getDashboardAggregatedCount({ ano_inicio: 1978 })
                }
                Loading={FaderWrapper}
              />
            </LineChartWrapper>
            <BarChartWrapper>
              <BarChartContainer
                labels={[instituteStyle.graphs.barChart.instituteLabel]}
                type="single"
                colors={barColors}
                fetchData={async () => ({
                  countByYearByCategoryByLabel: {
                    [instituteStyle.graphs.barChart.instituteLabel.replace(
                      /[""]/g,
                      ""
                    )]: countByProductionType,
                  },
                  categories: [
                    "Produção Artística",
                    "Produção Técnica",
                    "Produção Bibliográfica",
                    "Orientação",
                    "Bancas",
                    "Prêmios e Títulos",
                  ],
                })}
                Loading={FaderWrapper}
              />
            </BarChartWrapper>
            <WorldMapWrapper>
              <WorldMapContainer
                fetchData={() => getDashboardAggregatedMap()}
                Loading={FaderWrapper}
                color={instituteStyle.graphs.map.color}
              />
            </WorldMapWrapper>
            <NationalMapWrapper>
              <NationalMapContainer
                fetchData={() => getDashboardAggregatedNationalMap()}
                Loading={FaderWrapper}
                color={instituteStyle.graphs.map.color}
              />
            </NationalMapWrapper>
          </CarouselWrapper>
        </DivGraph>
      </div>
    </Conteudo>
  );
}

Home.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

HomeData.propTypes = {
  countByProductionType: PropTypes.object.isRequired,
  fontFamily: PropTypes.string,
  title: PropTypes.string.isRequired,
  titleColor: PropTypes.string,
  lastDataUpdateDate: PropTypes.string.isRequired,
  lastDataUpdateNoticeColor: PropTypes.string,
};

export default Home;
