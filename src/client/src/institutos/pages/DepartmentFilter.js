import React from "react";
import PropTypes from "prop-types";
import Department from "../components/department/Department";

function DepartmentFilter(props) {
  const {
    setMenuFilter,
    department,
    name,
    chefe,
    viceChefe,
    color,
    history,
    borderColor,
    borderWidth,
  } = props;
  return (
    <Department
      setMenuFilter={setMenuFilter}
      dep={department}
      depDescription={name}
      chefe={chefe}
      viceChefe={viceChefe}
      color={color}
      borderColor={borderColor}
      borderWidth={borderWidth}
    >
      <div
        dangerouslySetInnerHTML={{
          __html: history,
        }}
      ></div>
    </Department>
  );
}

DepartmentFilter.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
  department: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  chefe: PropTypes.string.isRequired,
  viceChefe: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  history: PropTypes.string.isRequired,
  borderColor: PropTypes.bool.isRequired,
  borderWidth: PropTypes.number.isRequired,
};

export default DepartmentFilter;
