import React from "react";
import PropTypes from "prop-types";
import Activity from "../components/activity/Activity";
import {
  getBancasCount,
  getBancasCountByType,
  getBancasKeywords,
  getBancasMap,
} from "../../common/API";
import { BancasDescription } from "../staticTexts";
import { setFilters } from "../helpers";

function Bancas(props) {
  const { setMenuFilter } = props;

  const updateMenuEntry = () => {
    setFilters(setMenuFilter, { bancas: true }, (filter, newState) => {
      newState[filter] = false;
    });
  };

  const chartsToRemove = ["nationalMap"];

  return (
    <Activity
      chartsToRemove={chartsToRemove}
      getCount={getBancasCount}
      getCountByType={getBancasCountByType}
      getKeywords={getBancasKeywords}
      getMap={getBancasMap}
      infoText={BancasDescription}
      type="Bancas"
      updateMenuEntry={updateMenuEntry}
    />
  );
}

Bancas.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default Bancas;
