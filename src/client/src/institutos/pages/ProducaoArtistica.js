import React from "react";
import PropTypes from "prop-types";
import Activity from "../components/activity/Activity";
import {
  getProdArtCount,
  getProdArtCountByType,
  getProdArtKeywords,
  getProdArtMap,
  getProdArtNationalMap,
} from "../../common/API";
import { ProdArtDescription } from "../staticTexts";
import { setFilters } from "../helpers";

function ProducaoArtistica(props) {
  const { setMenuFilter } = props;

  const updateMenuEntry = () => {
    setFilters(setMenuFilter, { artistica: true }, (filter, newState) => {
      newState[filter] = false;
    });
  };

  return (
    <Activity
      getCount={getProdArtCount}
      getCountByType={getProdArtCountByType}
      getKeywords={getProdArtKeywords}
      getMap={getProdArtMap}
      getNationalMap={getProdArtNationalMap}
      infoText={ProdArtDescription}
      type="Produção Artística"
      updateMenuEntry={updateMenuEntry}
    />
  );
}

ProducaoArtistica.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default ProducaoArtistica;
