/* eslint-disable react/jsx-key */
import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./pages/Home";
import HeaderWrapper from "./components/wrappers/HeaderWrapper";
import Menu from "./components/Menu";
import FooterWrapper from "./components/wrappers/FooterWrapper";
import Docentes from "./pages/Docentes";
import PerfilDocente from "./pages/PerfilDocente";
import ProducaoBibliografica from "./pages/ProducaoBibliografica";
import ProducaoArtistica from "./pages/ProducaoArtistica";
import ProducaoTecnica from "./pages/ProducaoTecnica";
import Orientacoes from "./pages/Orientacoes";
import Bancas from "./pages/Bancas";
import PremiosETitulos from "./pages/PremiosETitulos";
import PerfilProdBiblio from "./pages/PerfilProdBiblio";
import PerfilProdArt from "./pages/PerfilProdArt";
import PerfilProdTec from "./pages/PerfilProdTec";
import PerfilOrientacoes from "./pages/PerfilOrientacoes";
import PerfilBancas from "./pages/PerfilBancas";
import PerfilPremios from "./pages/PerfilPremios";
import FAQ from "./components/faq/FAQ";
import { Body } from "./styles";
import axios from "axios";
import InstituteStylesContext from "./context/InstituteStyles";
import "./fonts.scss";
import FaderWrapper from "./components/wrappers/FaderWrapper";
import DepartmentFilter from "./pages/DepartmentFilter";
import DevelopmentWarn from "../common/misc/DevelopmentWarn";

function App() {
  const [menuFilter, setMenuFilter] = useState({
    home: false,
    docentes: false,
    perfil: false,
    bibliografica: false,
    artistica: false,
    tecnica: false,
    orientacoes: false,
    bancas: false,
    premios: false,
    faq: false,
    unidade: true,
  });
  const [instituteStyles, setInstituteStyles] = useState({});
  const getStyles = async () => {
    const urlCMS = process.env.REACT_APP_CMS_URL;
    const instituteCMS = process.env.REACT_APP_INSTITUTE_CMS;
    const route = urlCMS + `/api/instituicao/${instituteCMS}`;
    console.log("route", route);
    axios.get(route).then((response) => {
      const ex = response.data;
      console.log(ex);
      setInstituteStyles(ex);
    });
  };

  useEffect(() => {
    if (instituteStyles.departments !== undefined) {
      document.querySelector("title").innerText = instituteStyles.titulo;
      document.querySelector(
        'link[rel="shortcut icon"]'
      ).href = `/${instituteStyles.nome}/favicon.ico`;
      let departments = {};
      Object.keys(instituteStyles.departments).map((key) => {
        departments[key] = false;
      });
      setMenuFilter({ ...menuFilter, ...departments });
    }
  }, [instituteStyles]);

  useEffect(() => {
    getStyles();
  }, []);
  if (instituteStyles.nome) {
    return (
      <Router>
        <InstituteStylesContext.Provider value={instituteStyles}>
          <DevelopmentWarn />
          <Body>
            <HeaderWrapper
              setMenuFilter={setMenuFilter}
              menuFilter={menuFilter}
            />
            <Menu menuFilter={menuFilter} />

            <Route path="/faq">
              <FAQ setMenuFilter={setMenuFilter} />
            </Route>

            <Switch>
              <Route exact path="/">
                <Home setMenuFilter={setMenuFilter} />
              </Route>
              {Object.keys(instituteStyles.departments).map((key) => {
                return (
                  <Route path={`/${key}`}>
                    <DepartmentFilter
                      setMenuFilter={setMenuFilter}
                      department={key}
                      name={instituteStyles.departments[key].name}
                      chefe={instituteStyles.departments[key].chefe}
                      viceChefe={instituteStyles.departments[key].viceChefe}
                      history={instituteStyles.departments[key].history}
                      color={instituteStyles.departments[key].color}
                      borderColor={instituteStyles.departments[key].borderColor}
                      borderWidth={instituteStyles.departments[key].borderWidth}
                    />
                  </Route>
                );
              })}
              <Route path="/producao-bibliografica">
                <ProducaoBibliografica setMenuFilter={setMenuFilter} />
              </Route>
              (instituteStyles.menu.prodArtistica ? (
              <Route path="/producao-artistica">
                <ProducaoArtistica setMenuFilter={setMenuFilter} />
              </Route>
              ) : null)
              <Route path="/producao-tecnica">
                <ProducaoTecnica setMenuFilter={setMenuFilter} />
              </Route>
              <Route path="/orientacoes">
                <Orientacoes setMenuFilter={setMenuFilter} />
              </Route>
              <Route path="/bancas">
                <Bancas setMenuFilter={setMenuFilter} />
              </Route>
              <Route path="/premios-e-titulos">
                <PremiosETitulos setMenuFilter={setMenuFilter} />
              </Route>
              <Route exact path="/pessoa/nomes">
                <Docentes setMenuFilter={setMenuFilter} department="all" />
              </Route>
              {Object.keys(instituteStyles.departments).map((key) => {
                return (
                  <Route path={`/pessoa/nomes/${key}`}>
                    <Docentes
                      setMenuFilter={setMenuFilter}
                      department={`${key}`}
                    />
                  </Route>
                );
              })}
              <Route path="/pessoa/perfil/:id">
                <PerfilDocente
                  menuFilter={menuFilter}
                  setMenuFilter={setMenuFilter}
                />
              </Route>
              <Route path="/pessoa/producao-bibliografica/:id">
                <PerfilProdBiblio
                  menuFilter={menuFilter}
                  setMenuFilter={setMenuFilter}
                />
              </Route>
              (instituteStyles.menu.prodArtistica ? (
              <Route path="/pessoa/producao-artistica/:id">
                <PerfilProdArt
                  menuFilter={menuFilter}
                  setMenuFilter={setMenuFilter}
                />
              </Route>
              ) : null)
              <Route path="/pessoa/producao-tecnica/:id">
                <PerfilProdTec
                  menuFilter={menuFilter}
                  setMenuFilter={setMenuFilter}
                />
              </Route>
              <Route path="/pessoa/bancas/:id">
                <PerfilBancas
                  menuFilter={menuFilter}
                  setMenuFilter={setMenuFilter}
                />
              </Route>
              <Route path="/pessoa/orientacoes/:id">
                <PerfilOrientacoes
                  menuFilter={menuFilter}
                  setMenuFilter={setMenuFilter}
                />
              </Route>
              <Route path="/pessoa/premios-e-titulos/:id">
                <PerfilPremios
                  menuFilter={menuFilter}
                  setMenuFilter={setMenuFilter}
                />
              </Route>
            </Switch>
            <FooterWrapper />
          </Body>
        </InstituteStylesContext.Provider>
      </Router>
    );
  } else {
    return <FaderWrapper />;
  }
}

export default App;
