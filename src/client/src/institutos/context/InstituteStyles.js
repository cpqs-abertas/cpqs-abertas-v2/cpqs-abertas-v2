import { createContext } from "react";

const InstituteStylesContext = createContext({});

export default InstituteStylesContext;
